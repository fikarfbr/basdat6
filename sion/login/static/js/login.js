$(document).ready(function(){

    function updateAddress(){
    var street = $("#street").val();
    try {
      var city = kotaDipilih['text'];
      var province = kotaDipilih['province'];
      var postal_code = $("#postal").val();
      $("#order-address").text(street + " , " + city + " , " + province + " , " + postal_code);
      $('input#alamat').val(street + " , " + city + " , " + province + " , " + postal_code);
    } catch (e) {
      $("#order-address").text(street);
      $('input#alamat').val(street);
    }
  }

  function updateAddress2(){
    var street = $("input#street2").val();
    try {
      var city = kotaDipilih2['text'];
      var province = kotaDipilih2['province'];
      var postal_code = $("input#postal2").val();
      $("#order-address2").text(street + " , " + city + " , " + province + " , " + postal_code);
      $('input#alamat2').val(street + " , " + city + " , " + province + " , " + postal_code);
    } catch (e) {
      $("#order-address2").text(street);
      $('input#alamat2').val(street);
    }
  }

  function updateAddress3(){
    var street = $("input#street3").val();
    try {
      var city = kotaDipilih3['text'];
      var province = kotaDipilih3['province'];
      var postal_code = $("input#postal3").val();
      $("#order-address3").text(street + " , " + city + " , " + province + " , " + postal_code);
      $('input#alamat3').val(street + " , " + city + " , " + province + " , " + postal_code);
    } catch (e) {
      $("#order-address3").text(street);
      $('input#alamat3').val(street);
    }
  }
  $( "#postal" ).keyup(function() {
    updateAddress();
});
  $( "#street" ).keyup(function() {
  updateAddress();
});

  $( "#postal2" ).keyup(function() {
    updateAddress2();
});
  $( "#street2" ).keyup(function() {
  updateAddress2();
});

  $( "#postal3" ).keyup(function() {
    updateAddress3();
});
  $( "#street3" ).keyup(function() {
  updateAddress3();
});
            $(".demo1 .rotate").textrotator({
        animation: "fade",
        speed: 1000
      });
   // Ajax untuk mendapat data provinsi dan ditampilkan dalam select province
    $.ajax({
        method: "GET",
        url: "/get_city/",
        async: false,
        success : function (data) {
            listCity = data['rajaongkir']['results'];
            for (var i = 0; i < listCity.length; i++) {
              listCity[i]['id'] = listCity[i]['city_id'];
              listCity[i]['text'] = listCity[i]['type'] + " " + listCity[i]['city_name'];
            }
            console.log(listCity);
            if (localStorage.getItem("listCity") === null) {
              localStorage.setItem("listCity", JSON.stringify(listCity));
            }
            $(".my-select-city").select2({
              placeholder: "Select Your Region",
              "data" : listCity
             });

            $(".my-select-city2").select2({
              placeholder: "Select Your Region",
              "data" : listCity
             });

            $(".my-select-city3").select2({
              placeholder: "Select Your Region",
              "data" : listCity
             });
        }
    });

             $(".my-select-city").on('change', function (e){
      var index;
      var id_kota = $(".my-select-city").val();
      for (var i = 0; i < listCity.length; i++){
        if (listCity[i]['id'] == id_kota){
          index = i;
          kotaDipilih = listCity[i]
          break;
        }
      }
      updateAddress();
    });

                 $(".my-select-city2").on('change', function (e){
      var index;
      var id_kota = $(".my-select-city2").val();
      for (var i = 0; i < listCity.length; i++){
        if (listCity[i]['id'] == id_kota){
          index = i;
          kotaDipilih2 = listCity[i]
          break;
        }
      }
      updateAddress2();
    });

                 $(".my-select-city3").on('change', function (e){
      var index;
      var id_kota = $(".my-select-city3").val();
      for (var i = 0; i < listCity.length; i++){
        if (listCity[i]['id'] == id_kota){
          index = i;
          kotaDipilih3 = listCity[i]
          break;
        }
      }
      updateAddress3();
    });

            });

/*
 * If u want integrate that, i've create a library for use ripple in your
 * beautifoul projects 🤙
 *
 * Here the link:
 * https://github.com/tomma5o/touchMyRipple
 */

const isMobile = window.navigator.userAgent.match(/Mobile/) && window.navigator.userAgent.match(/Mobile/)[0] === "Mobile";
const event = isMobile ? "touchstart" : "click";

const button = document.querySelectorAll('*[data-animation="ripple"]'),
            container = document.querySelector(".container");

for (var i = 0; i < button.length; i++) {
    const currentBtn = button[i];
    
    currentBtn.addEventListener(event, function(e) {
        
        e.preventDefault();
        const button = e.target,
                    rect = button.getBoundingClientRect(),
                    originalBtn = this,
                    btnHeight = rect.height,
                    btnWidth = rect.width;
        let posMouseX = 0,
                posMouseY = 0;
        
        if (isMobile) {
            posMouseX = e.changedTouches[0].pageX - rect.left;
            posMouseY = e.changedTouches[0].pageY - rect.top;
        } else {
            posMouseX = e.x - rect.left;
            posMouseY = e.y - rect.top;
        }
        
        const baseCSS =  `position: absolute;
                                            width: ${btnWidth * 2}px;
                                            height: ${btnWidth * 2}px;
                                            transition: all linear 700ms;
                                            transition-timing-function:cubic-bezier(0.250, 0.460, 0.450, 0.940);
                                            border-radius: 50%;
                                            background: var(--color-ripple);
                                            top:${posMouseY - btnWidth}px;
                                            left:${posMouseX - btnWidth}px;
                                            pointer-events: none;
                                            transform:scale(0)`
        
        var rippleEffect = document.createElement("span");
        rippleEffect.style.cssText = baseCSS;
        
        //prepare the dom
        currentBtn.style.overflow = "hidden";
        this.appendChild(rippleEffect);
        
        //start animation
        setTimeout( function() { 
            rippleEffect.style.cssText = baseCSS + `transform:scale(1); opacity: 0;`;
        }, 5);
        
        setTimeout( function() {
            rippleEffect.remove();
            //window.location.href = currentBtn.href;
        },700);
    })
}
