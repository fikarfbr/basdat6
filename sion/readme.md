### TK-5 Basis Data

- [ ] Seragam-in color scheme
- [ ] Seragam-in Navbar
- [ ] Konfigurasi Database 
- [ ] Sambungin Postgre-Django
- [ ] Bikin table (models.py)
- [ ] Bikin method di views.py masing masing fitur
- [ ] Integrasiin method di backend ke frontend
- [ ] Bikin validasi input (frontend dan backend)
- [ ] Bikin query buat transaksi data database
- [ ] Deploy